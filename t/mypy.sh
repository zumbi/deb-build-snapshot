#!/bin/sh
# Copyright © 2016-2018 Simon McVittie
# Copyright © 2018 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

MYPYPATH=$("${PYTHON:-python3}" -c '
import sys

path = []

for p in sys.path:
    if "-packages" in p or "/usr" not in p:
        path.append(p)

print(":".join(path))')
export MYPYPATH

if [ "x${MYPY:="$(command -v mypy || echo false)"}" = xfalse ]; then
    echo "1..0 # SKIP mypy not found"
elif "${MYPY}" \
        --python-executable="${PYTHON:=python3}" \
        --follow-imports=skip \
        deb-build-snapshot; then
    echo "1..1"
    echo "ok 1 - mypy reported no issues"
else
    echo "1..1"
    echo "not ok 1 # TODO mypy issues reported"
fi

# vim:set sw=4 sts=4 et:
